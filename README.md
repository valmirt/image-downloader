# MyImageDownloader App

This is the capstone for Android App Development Specialization. The idea of this
project is to show any web image, the user will paste the valid link of the image 
and the image will appear. If the user does not pass any links, this default 
[image](http://sfwallpaper.com/images/download-free-wallpapers-for-mobiles-15.jpg)
appears when the button is pressed. There will be three activities, one with the 
field for image link and the button to download the image, another activity will
show the download feedback and the last will show the image.